# Prova de Avaliação Técnica

Esta Prova consiste em desenvolver uma aplicação WEB feita na linguagem PHP para fim de avaliação Técnica no processo seletivo da [Vertex Digital](http://www.vertexdigital.co/).

# Aplicação

- A aplicação que será desenvolvida é uma agenda de contatos, com a funcionalidade de adição para novos contatos, que consiste minimamente em: nome, telefone, e-mail e CEP;

- O cep inserido deverá ser validado via REST com alguma API gratuíta e pública, ex: [VIA CEP](https://viacep.com.br/);

- A agenda deve conter um campo de filtro que faça busca em dois dados do contato, por exemplo, buscar tanto pelo nome e e-mail.

# Método de Avaliação

A prova possui um critério mínimo de requisitos que devem ser entregue, que são:

 - Capacidade de adicionar Contatos;

 - Buscar Contatos;

 - Persistência das informações de contato em banco de dados;

 - Persistência dos dados de endereço consultados;

 - Criação de um endpoint que retorne, em formato JSON, todos os contatos cadastrados. Esse endpoint será utilizado em uma chamada REST (GET) externa para o backend da aplicação. 
   Ex: http://IP:PORT/api/contacts 

A partir do momento que foi atigindo o critério mínimo o candidato tem direito de submeter a prova, porém caso o candidato queira adicionar funcionalidades, modificações em questões de layout, arquitetura da aplicação etc, isso será avaliado também.

# Itens Técnicos Obrigatórios

- O canditado deverá utilizar o [Framework PHP Laravel](https://laravel.com/) em ambiente [Docker](https://www.docker.com/);

- IMPORTANTE: Criar um README com as instruções que permitam prontamente compilar, testar e rodar o projeto.

# Itens Adicionais

Caso o candidato utilize algum dos itens a seguir conseguirá pontos adicionais na prova:

 - Utilização de Padrões de Projetos;

 - Utilização de Testes Automatizados.

# Duração da Prova

 - A prova terá duração informada via email;

 - Ressaltando que o avaliado tem o direito de entregar a qualquer momento o código contando que esteja no tempo hábil da prova.

# Dúvidas

 - Enviar um email para **y.neves@vertexdigital.co**

# Passos para entregar a Prova

 - Subir o código para um repositório público do candidato, no github ou bitbucket;

 - No final enviar um e-mail para **b.bayma@vertexdigital.co** com os seguintes dados:
   * Seu Nome Completo;
   * Link do repositório que contem o código da prova.